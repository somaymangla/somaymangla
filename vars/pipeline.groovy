#!/usr/bin/env groovy
def gitrepo(branch,repo){
git branch: "$branch" , url: "$repo"
}
def mvncmd(command){
    sh "mvn ${command}" 
    }
def codereport(){
    cobertura coberturaReportFile: '**target/site/cobertura/coverage.xml'
}
